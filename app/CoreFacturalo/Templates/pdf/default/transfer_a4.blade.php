@php
    $document_number = $document->prefix.'-'.str_pad($document->id, 8, '0', STR_PAD_LEFT);
@endphp
<html>
<head>
</head>
<body>
<table class="full-width">
    <tr>
        @if(!empty($company->logo))
            <td width="20%">
                <div class="company_logo_box">
                    <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo" style="max-width: 150px;">
                </div>
            </td>
        @else
            <td width="20%">
                <img src="data:{{mime_content_type(public_path('logo/logo.jpg'))}};base64, {{base64_encode(file_get_contents(public_path('logo/logo.jpg')))}}" class="company_logo" style="max-width: 150px" />
            </td>
        @endif
        <td width="50%" class="pl-3">
            <div class="text-left">
                <h4 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>

                <h6 style="text-transform: uppercase;">
                        {{ ($establishment->address !== '-')? $establishment->address : '' }}
                        {{ ($establishment->district_id !== '-')? ', '.$establishment->district->description : '' }}
                        {{ ($establishment->province_id !== '-')? ', '.$establishment->province->description : '' }}
                        {{ ($establishment->department_id !== '-')? '- '.$establishment->department->description : '' }}
                </h6>

                @isset($establishment->trade_address)
                    <h6>{{ ($establishment->trade_address !== '-')? 'D. Comercial: '.$establishment->trade_address : '' }}</h6>
                @endisset

                <h6>{{ ($establishment->telephone !== '-')? 'Central telefónica: '.$establishment->telephone : '' }}</h6>

                <h6>{{ ($establishment->email !== '-')? 'Email: '.$establishment->email : '' }}</h6>

                @isset($establishment->web_address)
                    <h6>{{ ($establishment->web_address !== '-')? 'Web: '.$establishment->web_address : '' }}</h6>
                @endisset

                @isset($establishment->aditional_information)
                    <h6>{{ ($establishment->aditional_information !== '-')? $establishment->aditional_information : '' }}</h6>
                @endisset
            </div>
        </td>
        <td width="30%" class="border-box py-4 px-2 text-center">
            <h5 class="text-center">{{ 'TRASLADO' }}</h5>
            <h3 class="text-center">{{ $document_number }}</h3>
        </td>
    </tr>
</table>
<table class="full-width mt-5">
    <tr>
        <td width="120px">FECHA DE EMISIÓN</td>
        <td width="8px">:</td>
        <td>{{$document->created_at->format('Y-m-d')}}</td>
    </tr>
    <tr>
        <td width="120px">HORA DE EMISIÓN</td>
        <td width="8px">:</td>
        <td>{{$document->created_at->format('H:i:s')}}</td>
    </tr>
    <tr>
        <td width="120px">MOTIVO</td>
        <td width="8px">:</td>
        <td>{{$document->description}}</td>
    </tr>
</table>

<br/>
<table class="full-width mt-10 mb-10">
    <thead class="">
    <tr class="bg-grey">
        <th class="border-top-bottom text-center py-2" width="7%">CANT.</th>
        <th class="border-top-bottom text-center py-2" width="10%">UNIDAD</th>
        <th class="border-top-bottom text-left py-2" width="33%">DESCRIPCIÓN</th>
        <th class="border-top-bottom text-left py-2" width="25%">ALMACEN ORIGEN</th>
        <th class="border-top-bottom text-left py-2" width="25%">ALMACEN DESTINO</th>
    </tr>
    </thead>
    <tbody  >
    @foreach($document->items as $row)
        <tr>
            <td class="text-center align-top">
                @if(((int)$row->quantity != $row->quantity))
                    {{ $row->quantity }}
                @else
                    {{ number_format($row->quantity, 0) }}
                @endif
            </td>

            <td class="text-center align-top">{{ $row->item->unit_type_id }}</td>

            <td class="text-left align-top">
                {!!$row->item->description!!}
                {!!$row->item->category->name!!}
                {!!$row->item->brand->name!!}
                {!!$row->item->model!!}
            </td>

            <td class="text-left align-top">
            {!!$row->warehouse->description!!}
            </td>

            <td class="text-left align-top">
            {!!$row->warehouse_destination->description!!}
            </td>
        </tr>
        <tr>
            <td colspan="7" class="border-bottom"></td>
        </tr>
    @endforeach

    </tbody>
</table>
<br>
<table class="full-width">
        <tr>
            <td width="65%" style="text-align: top; vertical-align: top;">
                <p style="text-transform: uppercase;" class="font-bold">Series (S<span style="text-transform: lowercase;" class="font-bold">i el producto lo requiere): </span></p>
                <br/>
            </td>
        </tr>
</table>
<br><br><br>
<table class="full-width">
    <tr>
        <td>
            <strong>Usuario que procesa:</strong>
        </td>
    </tr>
    <tr>
        <td>{{ $user->name }}</td>
    </tr>
</table>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<table class="full-width">
    <tr>
        <td width="10%"></td>
        <td width="35%" class="border-bottom"></td>
        <td width="10%"></td>
        <td width="35%" class="border-bottom"></td>
        <td width="10%"></td>
    </tr>
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td width="10%"></td>
        <td width="35%" class="text-center py-2">Entrega</td>
        <td width="10%"></td>
        <td width="35%" class="text-center py-2">Recepción</td>
        <td width="10%"></td>
    </tr>
</table>
</body>
</html>
