@php
    $establishment = $document->establishment;
    $document_number = 'IN-'.str_pad($document->number, 8, '0', STR_PAD_LEFT);
    $totalInLetters = \App\CoreFacturalo\Helpers\Number\NumberLetter::convertToLetter($document->total);
    $payments = $document->payments;
@endphp
<html>
<head>
    {{--<title>{{ $document_number }}</title>--}}
    {{--<link href="{{ $path_style }}" rel="stylesheet" />--}}
</head>
<body>
    <table class="full-width">
        <tr>
            @if(!empty($company->logo))
                <td width="20%">
                    <div class="company_logo_box">
                        <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo" style="max-width: 150px;">
                    </div>
                </td>
            @else
                <td width="20%">
                    <img src="data:{{mime_content_type(public_path('logo/logo.jpg'))}};base64, {{base64_encode(file_get_contents(public_path('logo/logo.jpg')))}}" class="company_logo" style="max-width: 150px" />
                </td>
            @endif
            <td width="50%" class="pl-3">
                <div class="text-left">
                    <h4 class="">{{ $company->name }}</h4>
                    <h5>{{ 'RUC '.$company->number }}</h5>
                    <h6 style="text-transform: uppercase;">
                        {{ ($establishment->address !== '-')? $establishment->address : '' }}
                        {{ ($establishment->district_id !== '-')? ', '.$establishment->district->description : '' }}
                        {{ ($establishment->province_id !== '-')? ', '.$establishment->province->description : '' }}
                        {{ ($establishment->department_id !== '-')? '- '.$establishment->department->description : '' }}
                    </h6>

                    @isset($establishment->trade_address)
                        <h6>{{ ($establishment->trade_address !== '-')? 'D. Comercial: '.$establishment->trade_address : '' }}</h6>
                    @endisset

                    <h6>{{ ($establishment->telephone !== '-')? 'Central telefónica: '.$establishment->telephone : '' }}</h6>

                    <h6>{{ ($establishment->email !== '-')? 'Email: '.$establishment->email : '' }}</h6>

                    @isset($establishment->web_address)
                        <h6>{{ ($establishment->web_address !== '-')? 'Web: '.$establishment->web_address : '' }}</h6>
                    @endisset

                    @isset($establishment->aditional_information)
                        <h6>{{ ($establishment->aditional_information !== '-')? $establishment->aditional_information : '' }}</h6>
                    @endisset
                </div>
            </td>
            <td width="30%" class="border-box py-4 px-2 text-center">
                <h5 class="text-center">{{ $document->income_type->description }}</h5>
                <h3 class="text-center">{{ $document_number }}</h3>
            </td>
        </tr>
    </table>
    <table class="full-width mt-5">
        <tr>
            <td width="120px">FECHA DE EMISIÓN</td>
            <td width="8px">:</td>
            <td>{{date('Y-m-d', strtotime($document->date_of_issue))}}</td>
        </tr>
        <tr>
            <td width="120px">HORA DE EMISIÓN</td>
            <td width="8px">:</td>
            <td>{{$document->time_of_issue}}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">CLIENTE:</td>
            <td style="vertical-align: top;">:</td>
            <td style="vertical-align: top;">
                {{ $document->customer }}
            </td>
        </tr>
    </table>

    @if ($payments)
    <br/>
    <strong>Guias de remisión</strong>
    @endif

    <table class="full-width mt-10 mb-10">
        <thead class="">
        <tr class="bg-grey">
            <th class="border-top-bottom text-center py-2" width="8%">#</th>
            <th class="border-top-bottom text-left py-2" width="30%">MÉTODO DE GASTO</th>
            <th class="border-top-bottom text-left py-2" width="30%">DESTINO</th>
            <th class="border-top-bottom text-left py-2" width="20%">REFERENCIA</th>
            <th class="border-top-bottom text-right py-2" width="12%">MONTO</th>
        </tr>
        </thead>
        <tbody>
            @foreach($payments as $paym)
            <tr>
                <td class="text-center align-top">
                    {{ $loop->iteration }}
                </td>
                <td class="text-left align-top">
                    {{ $paym->payment_method_type->description }}
                </td>
                <td class="text-left align-top">
                    {{ $paym->global_payment ? $paym->global_payment->destination_description : '' }}
                </td>
                <td class="text-left align-top">
                    {{ $paym->reference }}
                </td>
                <td class="text-right align-top">
                    {{ number_format($paym->payment, 2) }}
                </td>
            </tr>
            <tr>
                <td colspan="5" class="border-bottom"></td>
            </tr>
            @endforeach
            <tr>
                <td colspan="4" class="text-right font-bold">TOTAL: {{ $document->currency_type->symbol }}</td>
                <td class="text-right font-bold">{{ number_format($document->total, 2) }}</td>
            </tr>
        </tbody>
    </table>
    <table class="full-width">
        <tr>
            <td width="65%" style="text-align: top; vertical-align: top;">
                <p style="text-transform: uppercase;">Son: <span class="font-bold">{{ $totalInLetters }} {{ $document->currency_type->description }}</span></p>
                <br/>
            </td>
        </tr>
    </table>
    <br><br><br>
    <table class="full-width">
        <tr>
            <td>
                <strong>MOTIVO DE INGRESO: {{ $document->income_reason->description }} </strong>
            </td>
        </tr>
        <tr>
            <td>
                <strong>ESTADO: {{ $document->state_type->description }} </strong>
            </td>
        </tr>
    </table>
    <br><br><br>
    <table class="full-width">
        <tr>
            <td><strong>PAGOS:</strong></td>
        </tr>
        @foreach($document->items as $row)
            <tr>
                <td>&#8226; {{ $row->description }} - {{ $document->currency_symbol }} {{ $row->total }}</td>
            </tr>
        @endforeach
    </table>
    <br>
    <table class="full-width">
        <tr>
            <td>
                <strong>Usuario que recepciona:</strong>
            </td>
        </tr>
        <tr>
            <td>{{ $document->user->name }}</td>
        </tr>
    </table>
    <br><br><br><br>
    <br><br><br><br>
    <br><br><br><br>
    <table class="full-width">
        <tr>
            <td width="30%"></td>
            <td width="40%" class="border-bottom"></td>
            <td width="30%"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="30%"></td>
            <td width="40%" class="text-center">Firma de cliente</td>
            <td width="30%"></td>
        </tr>
    </table>
</body>
</html>
