@php
    $establishment = $document->establishment;
    $document_number = 'GASTO-'.str_pad($document->number, 8, '0', STR_PAD_LEFT);
    $totalInLetters = \App\CoreFacturalo\Helpers\Number\NumberLetter::convertToLetter($document->total);
    $payments = $document->payments;
    $supplier = $document->supplier;
@endphp
<html>
<head>
    {{--<title>{{ $document_number }}</title>--}}
    {{--<link href="{{ $path_style }}" rel="stylesheet" />--}}
</head>
<body>
    <table class="full-width text-center">
        <tr>
            @if($company->logo)
                <td width="20%">
                    <div class="text-center company_logo_box pt-5">
                        <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo_ticket contain">
                    </div>
                </td>
            @else
                <td width="20%">
                    <img src="data:{{mime_content_type(public_path('logo/logo.jpg'))}};base64, {{base64_encode(file_get_contents(public_path('logo/logo.jpg')))}}" class="company_logo" style="max-width: 150px" />
                </td>
            @endif
        </tr>
        <tr>
            <td width="50%">
                <div class="text-center">
                    <h4 class="">{{ $company->name }}</h4>
                    <h5>{{ 'RUC '.$company->number }}</h5>
                    <h6 style="text-transform: uppercase;">
                        {{ ($establishment->address !== '-')? $establishment->address : '' }}
                        {{ ($establishment->district_id !== '-')? ', '.$establishment->district->description : '' }}
                        {{ ($establishment->province_id !== '-')? ', '.$establishment->province->description : '' }}
                        {{ ($establishment->department_id !== '-')? '- '.$establishment->department->description : '' }}
                    </h6>

                    @isset($establishment->trade_address)
                        <h6>{{ ($establishment->trade_address !== '-')? 'D. Comercial: '.$establishment->trade_address : '' }}</h6>
                    @endisset

                    <h6>{{ ($establishment->telephone !== '-')? 'Central telefónica: '.$establishment->telephone : '' }}</h6>

                    <h6>{{ ($establishment->email !== '-')? 'Email: '.$establishment->email : '' }}</h6>

                    @isset($establishment->web_address)
                        <h6>{{ ($establishment->web_address !== '-')? 'Web: '.$establishment->web_address : '' }}</h6>
                    @endisset

                    @isset($establishment->aditional_information)
                        <h6>{{ ($establishment->aditional_information !== '-')? $establishment->aditional_information : '' }}</h6>
                    @endisset
                </div>
            </td>
        </tr><br>
        <tr>
            <td width="30%" class="border-top border-bottom py-2 px-2 text-center">
                <h5 class="text-center">{{ $document->expense_type->description }}</h5>
                <h3 class="text-center">{{ $document_number }}</h3>
            </td>
        </tr>
    </table>
    <table class="full-width">
        <tr >
            <td width="" class="pt-3"><p class="desc">F. Emisión:</p></td>
            <td width="" class="pt-3"><p class="desc">{{date('Y-m-d', strtotime($document->date_of_issue))}}</p></td>
        </tr>
        <tr>
            <td width="" ><p class="desc">H. Emisión:</p></td>
            <td width="" ><p class="desc">{{ $document->time_of_issue }}</p></td>
        </tr>

        <tr>
            <td style="vertical-align: top;">PROVEEDOR:</td>
            <td style="vertical-align: top;">
                {{ $supplier->number }} - {{ $supplier->name }}
                @if ($supplier->internal_code ?? false)
                <br>
                <small>{{ $supplier->internal_code ?? '' }}</small>
                @endif
            </td>
        </tr>
    </table>

    <table class="full-width mt-10 mb-10">
        <thead class="">
        <tr>
            <th class="border-top-bottom desc-9 text-left">#</th>
            <th class="border-top-bottom desc-9 text-left">MÉTODO DE GASTO</th>
            <th class="border-top-bottom desc-9 text-left">DESTINO</th>
            <th class="border-top-bottom desc-9 text-left">REFERENCIA</th>
            <th class="border-top-bottom desc-9 text-left">MONTO</th>
        </tr>
        </thead>
        <tbody>
        @foreach($payments as $paym)
            <tr>
                <td class="text-center desc-9 align-top font-bold">
                    PAGO-{{ $loop->iteration }}
                </td>
                <td class="text-left desc-9 align-top">
                    {{ $paym->expense_method_type->description }}
                </td>
                <td class="text-left desc-9 align-top">
                    {{ $paym->global_payment ? $paym->global_payment->destination_description : '' }}
                </td>
                <td class="text-left desc-9 align-top">
                {{ $paym->reference }}
                </td>
                <td class="text-right desc-9 align-top font-bold">
                {{ number_format($paym->payment, 2) }}
                </td>
            </tr>
            <tr>
                <td colspan="5" class="border-bottom"></td>
            </tr>
        @endforeach
            <tr>
                <td colspan="4" class="text-right font-bold desc-9">TOTAL: {{ $document->currency_type->symbol }}</td>
                <td class="text-right font-bold desc-9">{{ number_format($document->total, 2) }}</td>
            </tr>
        </tbody>
    </table>
    <table class="full-width small desc-9">
            <tr>
                <td colspan="5" style="text-align: top; vertical-align: top; font-size:8px;">
                    <p style="text-transform: uppercase;">Son: <span class="font-bold">{{ $totalInLetters }} {{ $document->currency_type->description }}</span></p>
                    <br/>
                </td>
            </tr>
    </table>
    <br>
    <table class="full-width desc-7">
        <tr>
            <td>
                <strong>MOTIVO DE GASTO: {{ $document->expense_reason->description }} </strong>
            </td>
        </tr>
        <tr>
            <td>
                <strong>ESTADO: {{ $document->state_type->description }} </strong>
            </td>
        </tr>
    </table>
    <table class="full-width desc-7">
        <tr>
            <td><strong>GASTOS:</strong></td>
        </tr>
        @foreach($document->items as $row)
            <tr>
                <td>&#8226; {{ $row->description }} - {{ $document->currency_symbol }} {{ $row->total }}</td>
            </tr>
        @endforeach
    </table>
    <table class="full-width desc-7">
        <tr>
            <td>
                <strong>Usuario que procesa:</strong>
            </td>
        </tr>
        <tr>
            <td>{{ $document->user->name }}</td>
        </tr>
    </table>
    <br><br><br>
    <table class="full-width desc-9">
        <tr>
            <td width="30%"></td>
            <td width="40%" class="border-bottom"></td>
            <td width="30%"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="30%"></td>
            <td width="40%" class="text-center">Firma de cliente</td>
            <td width="30%"></td>
        </tr>
    </table>
</body>
</html>
