<?php

namespace Modules\Inventory\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Http\Resources\TransferCollection;
use Modules\Inventory\Http\Resources\TransferResource;
use Modules\Inventory\Models\Inventory;
use Modules\Inventory\Traits\InventoryTrait;
use Modules\Inventory\Models\ItemWarehouse;
use Modules\Inventory\Models\Warehouse;
use Modules\Inventory\Models\InventoryTransfer;
use Modules\Inventory\Http\Requests\InventoryRequest;
use Modules\Inventory\Http\Requests\TransferRequest;

use App\Models\Tenant\Company;
use App\Models\Tenant\Configuration;
use App\CoreFacturalo\Helpers\Storage\StorageDocument;
use App\CoreFacturalo\Template;
use Mpdf\Mpdf;
use Mpdf\HTMLParserMode;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;

use Modules\Item\Models\ItemLot;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\User;

class TransferController extends Controller
{
    use InventoryTrait, StorageDocument;

    protected $transfer;
    protected $company;
    protected $establishment;
    protected $user;

    public function index()
    {
        return view('inventory::transfers.index');
    }

    public function create()
    {
       // $establishment_id = auth()->user()->establishment_id;
        //$current_warehouse = Warehouse::where('establishment_id', $establishment_id)->first();
        return view('inventory::transfers.form');

    }

    public function columns()
    {
        return [
            'created_at' => 'Fecha de emisión',
        ];
    }

    public function records(Request $request)
    {
        if($request->column)
        {
            $records = InventoryTransfer::with(['warehouse','warehouse_destination', 'inventory'])->where('created_at', 'like', "%{$request->value}%")->latest();
        }
        else{
            $records = InventoryTransfer::with(['warehouse','warehouse_destination', 'inventory'])->latest();

        }
        //return json_encode( $records );
        /*$records = Inventory::with(['item', 'warehouse', 'warehouse_destination'])
                            ->where('type', 2)
                            ->whereHas('warehouse_destination')
                            ->whereHas('item', function($query) use($request) {
                                $query->where('description', 'like', '%' . $request->value . '%');

                            })
                            ->latest();*/


        return new TransferCollection($records->paginate(config('tenant.items_per_page')));
    }


    public function tables()
    {
        return [
            //'items' => $this->optionsItemWareHouse(),
            'warehouses' => $this->optionsWarehouse()
        ];
    }

    public function record($id)
    {
        $record = new TransferResource(Inventory::findOrFail($id));

        return $record;
    }


   /* public function store(Request $request)
    {

        $result = DB::connection('tenant')->transaction(function () use ($request) {

            $id = $request->input('id');
            $item_id = $request->input('item_id');
            $warehouse_id = $request->input('warehouse_id');
            $warehouse_destination_id = $request->input('warehouse_destination_id');
            $stock = $request->input('stock');
            $quantity = $request->input('quantity');
            $detail = $request->input('detail');

            if($warehouse_id === $warehouse_destination_id) {
                return  [
                    'success' => false,
                    'message' => 'El almacén destino no puede ser igual al de origen'
                ];
            }
            if($stock < $quantity) {
                return  [
                    'success' => false,
                    'message' => 'La cantidad a trasladar no puede ser mayor al que se tiene en el almacén.'
                ];
            }

            $re_it_warehouse = ItemWarehouse::where([['item_id',$item_id],['warehouse_id', $warehouse_destination_id]])->first();

            if(!$re_it_warehouse) {
                return  [
                    'success' => false,
                    'message' => 'El producto no se encuentra registrado en el almacén destino.'
                ];
            }


            $inventory = Inventory::findOrFail($id);

            //proccess stock
            $origin_inv_kardex = $inventory->inventory_kardex->first();
            $origin_item_warehouse = ItemWarehouse::where([['item_id',$origin_inv_kardex->item_id],['warehouse_id', $origin_inv_kardex->warehouse_id]])->first();
            $origin_item_warehouse->stock += $inventory->quantity;
            $origin_item_warehouse->stock -= $quantity;
            $origin_item_warehouse->update();


            $destination_inv_kardex = $inventory->inventory_kardex->last();
            $destination_item_warehouse = ItemWarehouse::where([['item_id',$destination_inv_kardex->item_id],['warehouse_id', $destination_inv_kardex->warehouse_id]])->first();
            $destination_item_warehouse->stock -= $inventory->quantity;
            $destination_item_warehouse->update();


            $new_item_warehouse = ItemWarehouse::where([['item_id',$item_id],['warehouse_id', $warehouse_destination_id]])->first();
            $new_item_warehouse->stock += $quantity;
            $new_item_warehouse->update();

            //proccess stock

            //proccess kardex
            $origin_inv_kardex->quantity = -$quantity;
            $origin_inv_kardex->update();

            $destination_inv_kardex->quantity = $quantity;
            $destination_inv_kardex->warehouse_id = $warehouse_destination_id;
            $destination_inv_kardex->update();
            //proccess kardex

            $inventory->warehouse_destination_id = $warehouse_destination_id;
            $inventory->quantity = $quantity;
            $inventory->detail = $detail;


            $inventory->update();

            return  [
                'success' => true,
                'message' => 'Traslado actualizado con éxito'
            ];
        });

        return $result;
    }*/


    public function destroy($id)
    {

        DB::connection('tenant')->transaction(function () use ($id) {

            $record = Inventory::findOrFail($id);

            $origin_inv_kardex = $record->inventory_kardex->first();
            $destination_inv_kardex = $record->inventory_kardex->last();

            $destination_item_warehouse = ItemWarehouse::where([['item_id',$destination_inv_kardex->item_id],['warehouse_id', $destination_inv_kardex->warehouse_id]])->first();
            $destination_item_warehouse->stock -= $record->quantity;
            $destination_item_warehouse->update();

            $origin_item_warehouse = ItemWarehouse::where([['item_id',$origin_inv_kardex->item_id],['warehouse_id', $origin_inv_kardex->warehouse_id]])->first();
            $origin_item_warehouse->stock += $record->quantity;
            $origin_item_warehouse->update();

            $record->inventory_kardex()->delete();
            $record->delete();

        });


        return [
            'success' => true,
            'message' => 'Traslado eliminado con éxito'
        ];



    }

    public function stock ($item_id, $warehouse_id)
    {

       $row = ItemWarehouse::where([['item_id', $item_id],['warehouse_id', $warehouse_id]])->first();

       return [
           'stock' => ($row) ? $row->stock : 0
       ];

    }

    public function store(TransferRequest $request)
    {
        $result = DB::connection('tenant')->transaction(function () use ($request) {

            $row = InventoryTransfer::create([
                'description' => $request->description,
                'warehouse_id' => $request->warehouse_id,
                'warehouse_destination_id' => $request->warehouse_destination_id,
                'quantity' =>  count( $request->items ),
            ]);

            //$this->transfer = $row;
            //$this->transfer->items = [];

            foreach ($request->items as $it)
            {
                $inventory = new Inventory();
                $inventory->type = 2;
                $inventory->description = 'Traslado';
                $inventory->item_id = $it['id'];
                $inventory->warehouse_id = $request->warehouse_id;
                $inventory->warehouse_destination_id = $request->warehouse_destination_id;
                $inventory->quantity = $it['quantity'];
                $inventory->inventories_transfer_id = $row->id;
                //$this->transfer->items[] = $inventory;

                $inventory->save();

                foreach ($it['lots'] as $lot){

                    if($lot['has_sale']){
                        $item_lot = ItemLot::findOrFail($lot['id']);
                        $item_lot->warehouse_id = $inventory->warehouse_destination_id;
                        $item_lot->update();
                    }

                }
            }
            //$filename = "transferencia_".str_pad($row->id, 8, "0", STR_PAD_LEFT);
            //$this->createPdf($this->transfer, 'a4', $filename);

            return  [
                'success' => true,
                'message' => 'Traslado creado con éxito'
            ];
        });

        return $result;


    }


    public function items($warehouse_id)
    {
        return [
            'items' => $this->optionsItemWareHousexId($warehouse_id),
        ];
    }


    public function download($id, $format) {
        $this->transfer = InventoryTransfer::where('id', $id)->first();
        if(!$this->transfer) throw new Exception("El código {$id} es inválido, no se encontro el documento relacionado");
        $this->transfer->items = Inventory::where('inventories_transfer_id', $id)->get();
        $filename = "transferencia_".str_pad($id, 8, "0", STR_PAD_LEFT);
        $this->createPdf($this->transfer, 'a4', $filename);

        return $this->downloadStorage($filename, 'pdf');
    }

    public function createPdf($transfer = null, $format_pdf = null, $filename = null) {

        ini_set("pcre.backtrack_limit", "5000000");
        $template = new Template();
        $pdf = new Mpdf();

        $document = ($transfer != null) ? $transfer : $this->transfer;
        $company = ($this->company != null) ? $this->company : Company::active();
        $establishment = ($this->establishment != null) ? $this->establishment : Establishment::active();
        $user = ($this->user != null) ? $this->user : User::active();
        $filename = ($filename != null) ? $filename : ("transferencia_".str_pad($this->transfer->$id, 8, "0", STR_PAD_LEFT));

        // $base_template = config('tenant.pdf_template');
        $base_template = Configuration::first()->formats;
        $html = $template->pdfTransfer($base_template, "transfer", $company, $establishment, $user, $document, $format_pdf);

        $pdf_font_regular = config('tenant.pdf_name_regular');
        $pdf_font_bold = config('tenant.pdf_name_bold');

        if ($pdf_font_regular != false) {
            $defaultConfig = (new ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $pdf = new Mpdf([
                'fontDir' => array_merge($fontDirs, [
                    app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
                                                DIRECTORY_SEPARATOR.'pdf'.
                                                DIRECTORY_SEPARATOR.$base_template.
                                                DIRECTORY_SEPARATOR.'font')
                ]),
                'fontdata' => $fontData + [
                    'custom_bold' => [
                        'R' => $pdf_font_bold.'.ttf',
                    ],
                    'custom_regular' => [
                        'R' => $pdf_font_regular.'.ttf',
                    ],
                ]
            ]);
        }

        $path_css = app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
                                             DIRECTORY_SEPARATOR.'pdf'.
                                             DIRECTORY_SEPARATOR.$base_template.
                                             DIRECTORY_SEPARATOR.'style.css');

        $stylesheet = file_get_contents($path_css);

        $pdf->WriteHTML($stylesheet, HTMLParserMode::HEADER_CSS);
        $pdf->WriteHTML($html, HTMLParserMode::HTML_BODY);

        if ($format_pdf != 'ticket') {
            if(config('tenant.pdf_template_footer')) {
                $html_footer = $template->pdfFooter($base_template,$this->transfer);
                $pdf->SetHTMLFooter($html_footer);
            }
        }

        $this->uploadStorage($filename, $pdf->output('', 'S'), 'pdf');
    }

}
