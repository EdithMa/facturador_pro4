<?php

namespace Modules\Finance\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Finance\Models\Income;
use Modules\Finance\Models\IncomeReason;
use Modules\Finance\Models\IncomePayment;
use Modules\Finance\Models\IncomeType;
use Modules\Finance\Models\IncomeMethodType;
use Modules\Finance\Models\IncomeItem;
use Modules\Finance\Http\Resources\IncomeCollection;
use Modules\Finance\Http\Resources\IncomeResource;
use Modules\Finance\Http\Requests\IncomeRequest;
use Illuminate\Support\Str;
use App\Models\Tenant\Person;
use App\Models\Tenant\PaymentMethodType;
use App\Models\Tenant\Catalogs\CurrencyType;
use App\CoreFacturalo\Requests\Inputs\Common\PersonInput;
use App\Models\Tenant\Establishment;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Company;
use Modules\Finance\Traits\FinanceTrait;

use App\Models\Tenant\Configuration;
use App\CoreFacturalo\Helpers\Storage\StorageDocument;
use App\CoreFacturalo\Template;
use Mpdf\Mpdf;
use Mpdf\HTMLParserMode;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;

class IncomeController extends Controller
{

    use FinanceTrait, StorageDocument;

    protected $income;
    protected $company;

    public function index()
    {
        return view('finance::income.index');
    }


    public function create()
    {
        return view('finance::income.form');
    }

    public function columns()
    {
        return [
            'number' => 'Número',
            'date_of_issue' => 'Fecha de emisión',
        ];
    }


    public function records(Request $request)
    {
        $records = Income::where($request->column, 'like', "%{$request->value}%")
                            ->whereTypeUser()
                            ->latest();

        return new IncomeCollection($records->paginate(config('tenant.items_per_page')));
    }

    public function tables()
    {
        $establishment = Establishment::where('id', auth()->user()->establishment_id)->first();
        $currency_types = CurrencyType::whereActive()->get();
        $income_types = IncomeType::get();
        $payment_method_types = PaymentMethodType::all();
        $income_reasons = IncomeReason::all();
        $payment_destinations = $this->getPaymentDestinations();

        return compact('establishment','currency_types', 'income_types', 'payment_method_types', 'income_reasons', 'payment_destinations');
    }



    public function record($id)
    {
        $record = new IncomeResource(Income::findOrFail($id));
        return $record;
    }

    public function store(IncomeRequest $request)
    {

        $data = self::merge_inputs($request);

        $income = DB::connection('tenant')->transaction(function () use ($data) {

            $doc = Income::create($data);

            foreach ($data['items'] as $row)
            {
                $doc->items()->create($row);
            }

            foreach ($data['payments'] as $row)
            {
                $record_payment = $doc->payments()->create($row);
                $this->createGlobalPayment($record_payment, $row);
            }

            return $doc;
        });

        $this->income = new IncomeResource(Income::findOrFail($income->id));
        $fileName = "IN-".str_pad($income->id, 8, "0", STR_PAD_LEFT);
        $this->createPdf($this->income, 'a4', $fileName);

        return [
            'success' => true,
            'data' => [
                'id' => $income->id
            ],
        ];
    }

    public function download($id, $format){
        $income = Income::findOrFail($id);
        if(!$income) throw new Exception("El código {$id} es inválido, no se encuentró el documento relacionado");
        $fileName = "IN-".str_pad($income->id, 8, "0", STR_PAD_LEFT);
        return $this->downloadStorage($fileName, 'pdf');
    }

    private function createPdf($income = null, $format_pdf = null, $filename = null) {

        ini_set("pcre.backtrack_limit", "5000000");
        $template = new Template();
        $pdf = new Mpdf();

        $document = ($income != null) ? $income : $this->income;
        $company = ($this->company != null) ? $this->company : Company::active();
        $filename = ($filename != null) ? $filename : ("IN-".str_pad($this->income->id, 8, "0", STR_PAD_LEFT));
        $base_template = Configuration::first()->formats;
        $html = $template->pdf($base_template, "income", $company, $document, $format_pdf);

        $pdf_font_regular = config('tenant.pdf_name_regular');
        $pdf_font_bold = config('tenant.pdf_name_bold');

        if ($pdf_font_regular != false) {
            $defaultConfig = (new ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $pdf = new Mpdf([
                'fontDir' => array_merge($fontDirs, [
                    app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
                                                DIRECTORY_SEPARATOR.'pdf'.
                                                DIRECTORY_SEPARATOR.$base_template.
                                                DIRECTORY_SEPARATOR.'font')
                ]),
                'fontdata' => $fontData + [
                    'custom_bold' => [
                        'R' => $pdf_font_bold.'.ttf',
                    ],
                    'custom_regular' => [
                        'R' => $pdf_font_regular.'.ttf',
                    ],
                ]
            ]);
        }

        $path_css = app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
                                             DIRECTORY_SEPARATOR.'pdf'.
                                             DIRECTORY_SEPARATOR.$base_template.
                                             DIRECTORY_SEPARATOR.'style.css');

        $stylesheet = file_get_contents($path_css);

        $pdf->WriteHTML($stylesheet, HTMLParserMode::HEADER_CSS);
        $pdf->WriteHTML($html, HTMLParserMode::HTML_BODY);

        if ($format_pdf != 'ticket') {
            if(config('tenant.pdf_template_footer')) {
                $html_footer = $template->pdfFooter($base_template,$this->income);
                $pdf->SetHTMLFooter($html_footer);
            }
        }
        //public function uploadStorage($filename, $file_content, $file_type, $root = null)
        $this->uploadStorage($filename, $pdf->output('', 'S'), 'pdf');
    }

    public static function merge_inputs($inputs)
    {

        $company = Company::active();

        $values = [
            'user_id' => auth()->id(),
            'number' => $inputs['id'] ? $inputs['number'] : self::newNumber($company->soap_type_id),
            'state_type_id' => '05',
            'soap_type_id' => $company->soap_type_id,
            'external_id' => Str::uuid()->toString(),
        ];

        $inputs->merge($values);

        return $inputs->all();
    }

    private static function newNumber($soap_type_id){

        $number = Income::select('number')
                            ->where('soap_type_id', $soap_type_id)
                            ->max('number');

        return ($number) ? (int)$number+1 : 1;

    }

    public function table($table)
    {
        switch ($table) {
            case 'suppliers':

                $suppliers = Person::whereType('suppliers')->orderBy('name')->get()->transform(function($row) {
                    return [
                        'id' => $row->id,
                        'description' => $row->number.' - '.$row->name,
                        'name' => $row->name,
                        'number' => $row->number,
                        'identity_document_type_id' => $row->identity_document_type_id,
                        'identity_document_type_code' => $row->identity_document_type->code
                    ];
                });
                return $suppliers;

                break;
            default:

                return [];

                break;
        }
    }

    public function voided($id)
    {

        $income = Income::findOrFail($id);
        $income->state_type_id = 11;
        $income->save();

        return [
            'success' => true,
            'message' => 'Ingreso anulado exitosamente',
        ];
    }


}
