<?php

namespace Modules\Expense\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Expense\Models\Expense;
use Modules\Expense\Models\ExpenseReason;
use Modules\Expense\Models\ExpensePayment;
use Modules\Expense\Models\ExpenseType;
use Modules\Expense\Models\ExpenseMethodType;
use Modules\Expense\Models\ExpenseItem;
use Modules\Expense\Http\Resources\ExpenseCollection;
use Modules\Expense\Http\Resources\ExpenseResource;
use Modules\Expense\Http\Requests\ExpenseRequest;
use Illuminate\Support\Str;
use App\Models\Tenant\Person;
use App\Models\Tenant\Catalogs\CurrencyType;
use App\CoreFacturalo\Requests\Inputs\Common\PersonInput;
use App\Models\Tenant\Establishment;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Company;
use Modules\Finance\Traits\FinanceTrait;
use Modules\Expense\Exports\ExpenseExport;
use Carbon\Carbon;

use App\Models\Tenant\Configuration;
use App\CoreFacturalo\Helpers\Storage\StorageDocument;
use App\CoreFacturalo\Template;
use Mpdf\Mpdf;
use Mpdf\HTMLParserMode;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;

use App\Models\Tenant\Document;

class ExpenseController extends Controller
{

    use FinanceTrait, StorageDocument;

    protected $expense;
    protected $company;

    public function index()
    {
        return view('expense::expenses.index');
    }


    public function create($id = null)
    {
        return view('expense::expenses.form', compact('id'));
    }

    public function columns()
    {
        return [
            'date_of_issue' => 'Fecha de emisión',
            'number' => 'Número',
        ];
    }


    public function records(Request $request)
    {
        $records = Expense::where($request->column, 'like', "%{$request->value}%")
                            ->whereTypeUser()
                            ->latest();

        return new ExpenseCollection($records->paginate(config('tenant.items_per_page')));
    }

    public function tables()
    {
        $suppliers = $this->table('suppliers');
        $establishment = Establishment::where('id', auth()->user()->establishment_id)->first();
        $currency_types = CurrencyType::whereActive()->get();
        $expense_types = ExpenseType::get();
        $expense_method_types = ExpenseMethodType::all();
        $expense_reasons = ExpenseReason::all();
        $payment_destinations = $this->getBankAccounts();

        return compact('suppliers', 'establishment','currency_types', 'expense_types', 'expense_method_types', 'expense_reasons', 'payment_destinations');
    }



    public function record($id)
    {
        $record = new ExpenseResource(Expense::findOrFail($id));
        return $record;
    }

    public function store(ExpenseRequest $request)
    {

        $data = self::merge_inputs($request);
        // dd($data);

        $expense = DB::connection('tenant')->transaction(function () use ($data) {

            // $doc = Expense::create($data);
            $doc = Expense::updateOrCreate(['id' => $data['id']], $data);

            $doc->items()->delete();

            foreach ($data['items'] as $row)
            {
                $doc->items()->create($row);
            }

            $this->deleteAllPayments($doc->payments);

            foreach ($data['payments'] as $row)
            {
                $record_payment = $doc->payments()->create($row);

                if($row['expense_method_type_id'] == 1){
                    $row['payment_destination_id'] = 'cash';
                }

                $this->createGlobalPayment($record_payment, $row);
            }

            return $doc;
        });

        $this->expense = new ExpenseResource(Expense::findOrFail($expense->id));
        $fileName = "GASTO-".str_pad($expense->id, 8, "0", STR_PAD_LEFT);
        $this->createPdf($this->expense, 'a4', $fileName);

        return [
            'success' => true,
            'data' => [
                'id' => $expense->id,
            ],
        ];
    }


    public function download($id, $format){
        $expense = Expense::findOrFail($id);
        if(!$expense) throw new Exception("El código {$id} es inválido, no se encontró el documento relacionado");
        $fileName = "GASTO-".str_pad($expense->id, 8, "0", STR_PAD_LEFT);
        $this->reloadPDF($expense, $format, $fileName);
        return $this->downloadStorage($fileName, 'pdf');
    }

    private function reloadPDF($expense, $format, $fileName) {
        $this->createPdf($expense, $format, $fileName);
    }

    private function createPdf($expense = null, $format_pdf = null, $filename = null) {

        ini_set("pcre.backtrack_limit", "5000000");
        $template = new Template();
        $pdf = new Mpdf();

        $document = ($expense != null) ? $expense : $this->expense;
        $company = ($this->company != null) ? $this->company : Company::active();
        //$filename = ($filename != null) ? $filename : ("IN-".str_pad($this->expense->id, 8, "0", STR_PAD_LEFT));
        $filename = ($filename != null) ? $filename : $this->expense->filename;
        $base_template = Configuration::first()->formats;
        $html = $template->pdf($base_template, "expense", $company, $document, $format_pdf);

        if ($format_pdf === 'ticket' or $format_pdf === 'ticket_80') {

            $width = 78;
            $pdf_margin_top = 2;
            $pdf_margin_right = 5;
            $pdf_margin_bottom = 0;
            $pdf_margin_left = 5;
            if (config('tenant.enabled_template_ticket_80')) $width = 76;

            $company_name = (strlen($company->name) / 20) * 10;
            $company_address = (strlen($document->establishment->address) / 30) * 10;
            $company_number = $document->establishment->telephone != '' ? '10' : '0';
            //$customer_name = strlen($document->customer->name) > '25' ? '10' : '0';
            //$customer_address = (strlen($document->customer->address) / 200) * 10;
            $p_order = $document->purchase_order != '' ? '10' : '0';

            $total_exportation = $document->total_exportation != '' ? '10' : '0';
            $total_free = $document->total_free != '' ? '10' : '0';
            $total_unaffected = $document->total_unaffected != '' ? '10' : '0';
            $total_exonerated = $document->total_exonerated != '' ? '10' : '0';
            $total_taxed = $document->total_taxed != '' ? '10' : '0';
            $quantity_rows = count($document->items);
            $discount_global = 0;
            foreach ($document->items as $it) {
                if ($it->discounts) {
                    $discount_global = $discount_global + 1;
                }
            }
            $legends = $document->legends != '' ? '10' : '0';

            $pdf = new Mpdf([
                                'mode'          => 'utf-8',
                                'format'        => [
                                    $width,
                                    120 +
                                    ($quantity_rows * 30) +
                                    ($discount_global * 6) +
                                    $company_name +
                                    $company_address +
                                    $company_number +
                                    //$customer_name +
                                    //$customer_address +
                                    $p_order +
                                    $legends +
                                    $total_exportation +
                                    $total_free +
                                    $total_unaffected +
                                    $total_exonerated +
                                    $total_taxed],
                                'margin_top'    => $pdf_margin_top,
                                'margin_right'  => $pdf_margin_right,
                                'margin_bottom' => $pdf_margin_bottom,
                                'margin_left'   => $pdf_margin_left,
                            ]);
        }else {

            $pdf_font_regular = config('tenant.pdf_name_regular');
            $pdf_font_bold = config('tenant.pdf_name_bold');

            if ($pdf_font_regular != false) {
                $defaultConfig = (new ConfigVariables())->getDefaults();
                $fontDirs = $defaultConfig['fontDir'];

                $defaultFontConfig = (new FontVariables())->getDefaults();
                $fontData = $defaultFontConfig['fontdata'];

                $pdf = new Mpdf([
                    'fontDir' => array_merge($fontDirs, [
                        app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
                                                    DIRECTORY_SEPARATOR.'pdf'.
                                                    DIRECTORY_SEPARATOR.$base_template.
                                                    DIRECTORY_SEPARATOR.'font')
                    ]),
                    'fontdata' => $fontData + [
                        'custom_bold' => [
                            'R' => $pdf_font_bold.'.ttf',
                        ],
                        'custom_regular' => [
                            'R' => $pdf_font_regular.'.ttf',
                        ],
                    ]
                ]);
            }
        }

        $path_css = app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
                                             DIRECTORY_SEPARATOR.'pdf'.
                                             DIRECTORY_SEPARATOR.$base_template.
                                             DIRECTORY_SEPARATOR.'style.css');

        $stylesheet = file_get_contents($path_css);

        $pdf->WriteHTML($stylesheet, HTMLParserMode::HEADER_CSS);
        $pdf->WriteHTML($html, HTMLParserMode::HTML_BODY);

        if ($format_pdf != 'ticket') {
            if(config('tenant.pdf_template_footer')) {
                $html_footer = $template->pdfFooter($base_template,$this->expense);
                $pdf->SetHTMLFooter($html_footer);
            }
        }
        $this->uploadStorage($filename, $pdf->output('', 'S'), 'pdf');
    }

    public static function merge_inputs($inputs)
    {

        $company = Company::active();

        $values = [
            'user_id' => auth()->id(),
            'state_type_id' => $inputs['id'] ? $inputs['state_type_id'] : '05',
            'soap_type_id' => $company->soap_type_id,
            'external_id' => $inputs['id'] ? $inputs['external_id'] : Str::uuid()->toString(),
            'supplier' => PersonInput::set($inputs['supplier_id']),
        ];

        $inputs->merge($values);

        return $inputs->all();
    }

    public function table($table)
    {
        switch ($table) {
            case 'suppliers':

                $suppliers = Person::whereType('suppliers')->orderBy('name')->get()->transform(function($row) {
                    return [
                        'id' => $row->id,
                        'description' => $row->number.' - '.$row->name,
                        'name' => $row->name,
                        'number' => $row->number,
                        'identity_document_type_id' => $row->identity_document_type_id,
                        'identity_document_type_code' => $row->identity_document_type->code
                    ];
                });
                return $suppliers;

                break;
            default:

                return [];

                break;
        }
    }

    public function voided ($record)
    {
        try {
            $expense = Expense::findOrFail($record);
            $expense->state_type_id = 11;
            $expense->save();
            return [
                'success' => true,
                'data' => [
                    'id' => $expense->id,
                ],
                'message' => 'Gasto anulado exitosamente',
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [
                    'id' => $record,
                ],
                'message' => 'Falló al anular',
            ];
        }
    }

    public function excel(Request $request) {

        $records = Expense::where($request->column, 'like', "%{$request->value}%")
                            ->whereTypeUser()
                            ->latest()
                            ->get();
        // dd($records);

        $establishment = auth()->user()->establishment;
        $balance = new ExpenseExport();
        $balance
            ->records($records)
            ->establishment($establishment);

        // return $balance->View();
        return $balance->download('Expense_'.Carbon::now().'.xlsx');

    }

}
